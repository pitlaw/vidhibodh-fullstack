from lxml import etree as et
from nltk.tokenize import TabTokenizer
import copy


list_depth = 0
ref_dict = {}
num_style = {
  1: "D1Num",
  2: "D2Num",
  3: "D3Num",
  4: "D4Num"
}
text_style = {
  1: "D1Text",
  2: "D2Text",
  3: "D3Text",
  4: "D4Text"
}

def get_list_depth():
    global list_depth
    return list_depth


def set_list_depth(depth):
    global list_depth
    list_depth = depth


def inc_list_depth():
    global list_depth
    list_depth = list_depth + 1


def dec_list_depth():
    global list_depth
    list_depth = list_depth - 1


def get_num_style():
    global num_style
    return num_style[get_list_depth()]


def get_text_style():
    global text_style
    return text_style[get_list_depth()]


def add_footnotes(root, footnote):
    #  4 kinds of footnotes
    a_dict = {} # A Textual modifications generally through amendments
    m_dict = {} # M Non-textual modifications
    i_dict = {} # I Commencement informatin
    c_dict = {} # C Corrigendum

    # head = et.SubElement(footnote, 'head')
    # link = et.SubElement(head, 'link')
    # link.set("rel", "stylesheet")
    # link.set("href", "akn-exp.css")
    # body = et.SubElement(footnote, 'body')
    # div_leg = et.SubElement(body, 'div')
    # div_leg.set("class", "LegFootnote")

    note_list = root.findall('.//note')
    for note in note_list:
        eid = note.get('eId')
        note_class = note.get('class')
        tokens = note_class.split()
        if tokens[0] != 'commentary':
            continue

        expr = "//noteRef[@href=$name]"
        noteref = root.xpath(expr, name=eid)
        # No links are to be added for I and M
        # Links for A and C types
        if len(noteref) > 0:
            marker = noteref[0].get('marker')
            p = et.Element('p')
            p.set('id', eid)
            span_marker = et.SubElement(p, 'span')
            span_content = et.SubElement(p, 'span')
            span_marker.set('class', 'LHS')
            span_content.set('class', 'RHS')
            # For A and C type, add  links
            if tokens[1] == 'A' or tokens[1] == 'C':
                marker_a = et.SubElement(span_marker, 'a')
                marker_a.set('href', '#'+marker)
                marker_a.text = marker
            else:
                span_marker.text = marker
            # span_content.text = note.find('p').text
            # Assuming note has only one p and p has many ref
            note_p = copy.deepcopy(note.find('p'))
            span_content.text = note_p.text

            # Add all children of p; for ref, replace with a
            for child in note_p:
                if child.tag == 'ref':
                    a = et.Element('a')
                    a.set('href', ref_dict[child.get('href').strip()])
                    a.set('target', '_blank')
                    a.text = child.text
                    a.tail = child.tail
                    span_content.append(a)
                else:
                    span_content.append(child)

            if tokens[1] == 'A':
                a_dict[int(noteref[0].get('marker')[1:])] = p
            elif tokens[1] == 'M':
                m_dict[int(noteref[0].get('marker')[1:])] = p
            elif tokens[1] == 'I':
                i_dict[int(noteref[0].get('marker')[1:])] = p
            elif tokens[1] == 'C':
                c_dict[int(noteref[0].get('marker')[1:])] = p

        else:
            print("Skipping note as no noteRef for eID: %s", (eid))

    if c_dict:
        c_div = et.SubElement(footnote, 'div')
        et.SubElement(c_div, 'h4').text = "Corrigendum"
        for key in sorted(c_dict):
            c_div.append(c_dict[key])
    if i_dict:
        i_div = et.SubElement(footnote, 'div')
        et.SubElement(i_div, 'h4').text = "Commencement Information"
        for key in sorted(i_dict):
            i_div.append(i_dict[key])
    if m_dict:
        m_div = et.SubElement(footnote, 'div')
        et.SubElement(m_div, 'h4').text = "Modifications not altering text"
        for key in sorted(m_dict):
            m_div.append(m_dict[key])
    if a_dict:
        a_div = et.SubElement(footnote, 'div')
        et.SubElement(a_div, 'h4').text = "Modifications altering texts"
        for key in sorted(a_dict):
            a_div.append(a_dict[key])


def ref_resolver(ref_map_file_path):
    # ref_map_file_path = "F:/coding/xml/50of2017/refmap.txt"
    ref_map_file = open(ref_map_file_path, "r", encoding="utf8")
    lines = ref_map_file.readlines()
    tk = TabTokenizer()
    i = 0
    for line in lines:
        i = i + 1
        tokens = tk.tokenize(line)

        if len(tokens) == 2:
            ref_dict[tokens[0].strip()] = tokens[1].strip()
        else:
            print("Error at at line %d having %d tokens" % (i, len(tokens)))
    print(ref_dict)
    ref_map_file.close()


def start_parsing(root, out):
    # head = et.SubElement(out, 'head')
    # link = et.SubElement(head, 'link')
    # link.set("rel", "stylesheet")
    # link.set("href", "akn-exp.css")
    # body = et.SubElement(out, 'body')
    # div_leg = et.SubElement(body, 'div')
    # div_leg.set("class", "LegSnippet")
    # # body = et.SubElement(out, 'body')
    apply_template(root, out)

    # div_footnotes = et.SubElement(out, 'div')
    # add_footnotes(root, div_footnotes)
    return out


def apply_template(node, out):
    # if len(node) == 0:
    out.text = node.text
    out.tail = node.tail
    # return

    for child in node:
        select_template(child, out)


def select_template(node, out):
    if node.tag == 'doc':
        doc_template(node, out)

    if node.tag == 'meta':
        meta_template(node, out)

    if node.tag == 'identification':
        identification_template(node, out)

    if node.tag == 'notes':
        notes_template(node, out)

    if node.tag == 'preface':
        preface_template(node, out)

    if node.tag == 'preamble':
        preamble_template(node, out)

    if node.tag == 'mainBody':
        mainbody_template(node, out)

    if node.tag == 'container':
        container_template(node, out)

    if node.tag == 'paragraph':
        para_template(node, out)

    if node.tag == 'subparagraph':
        para_template(node, out)

    if node.tag == 'content':
        content_template(node, out)

    if node.tag == 'blockList':
        blocklist_template(node, out)

    if node.tag == 'item':
        item_template(node, out)

    if node.tag == 'num':
        num_template(node, out)

    if node.tag == 'listIntroduction':
        listintroduction_template(node, out)

    if node.tag == 'listWrapUp':
        listwrapup_template(node, out)

    if node.tag == 'p':
        p_template(node, out)

    if node.tag == 'i' or node.tag == 'b' or \
            node.tag == 'u' or node.tag == 'sup' \
            or node.tag == 'span':
        elem_template(node, out)

    if node.tag == 'table' or node.tag == 'thead' or \
            node.tag == 'tbody' or node.tag == 'tr' or \
            node.tag == 'td' or node.tag == 'th':
        table_elem_template(node, out)

    if node.tag == 'ins':
        ins_template(node, out)

    if node.tag == 'noteRef':
        noteref_template(node, out)


def doc_template(node, out):
    apply_template(node, out)


def meta_template(node, out):
    apply_template(node, out)


def identification_template(node, out):
    apply_template(node, out)


def notes_template(node, out):
    apply_template(node, out)


def preface_template(node, out):
    apply_template(node, out)


def preamble_template(node, out):
    apply_template(node, out)


def mainbody_template(node, out):
    apply_template(node, out)


def ins_template(node, out):
    #  two scenarios: 1. could have noteref 2. without noteref
    # Assuming no other nodes
    ins_type = node.get('class')
    tokens = ins_type.split()

    if len(tokens) == 3:
        if (tokens[1] == 'first') and (tokens[2] == 'last'):
            span1 = et.SubElement(out, 'span')
            span2 = et.SubElement(out, 'span')
            span3 = et.SubElement(out, 'span')
            span1.text = '['
            span2.text = node.text
            span3.text = ']'
            span3.tail = node.tail
            if len(node) != 0 and node[0].tag == 'noteRef':
                noteref_template(node[0], span2)

    elif len(tokens) == 2:
        span1 = et.SubElement(out, 'span')
        span2 = et.SubElement(out, 'span')
        if tokens[1] == 'first':
            span1.text = '['
            span2.text = node.text
            span2.tail = node.tail
            if len(node) != 0 and node[0].tag == 'noteRef':
                noteref_template(node[0], span2)

        if tokens[1] == 'last':
            span1.text = node.text
            span2.text = ']'
            span2.tail = node.tail
            if len(node) != 0 and node[0].tag == 'noteRef':
                noteref_template(node[0], span1)

    else:
        print('Unknown type of ins node found.')

def noteref_template(node, out):
    # assuming noteref to always just have tail and
    # marker, href and class attributes
    # Add marker only if type A and C
    noteref_class = node.get('class')
    tokens = noteref_class.split()
    span1 = et.SubElement(out, 'span')
    if tokens[0] == 'commentary' and (tokens[1] == 'A' or tokens[1] == 'C'):
        span1.set('id', node.get('marker'))
        sup = et.SubElement(span1, 'sup')
        b = et.SubElement(sup, 'b')
        a = et.SubElement(b, 'a')
        a.set('href', '#'+node.get('href'))
        a.text = node.get('marker')
    span1.tail = node.tail

def container_template(node, out):
    apply_template(node, out)


def para_template(node, out):
    inc_list_depth()
    apply_template(node, out)
    dec_list_depth()


def content_template(node, out):
    apply_template(node, out)

def blocklist_template(node, out):
    inc_list_depth()
    # div = et.SubElement(out, 'div')
    # div.set('class', "BlockList")
    apply_template(node, out)
    dec_list_depth()


def item_template(node, out):
    # apply_template(node, out)
    # numbered item
    if node[0].tag == 'num':
        p = et.SubElement(out, 'p')
        num_template(node[0], p)
        node_index = 1
        if len(node) > 1 and node[1].tag == 'p':
            span = et.SubElement(p, 'span')
            class_text = get_text_style() + ' ' + 'RHS'
            span.set('class', class_text)
            apply_template(node[1], span)
            node_index = 2
        for i in range(node_index, len(node)):
            select_template(node[i], out)
    # not a numbered item
    elif node[0].tag == 'p':
        p = et.SubElement(out, 'p')
        span = et.SubElement(p, 'span')
        class_text = 'NoNumText'
        span.set('class', class_text)
        apply_template(node[0], span)

        if len(node) > 1:
            for i in range(1, len(node)):
                select_template(node[i], out)

    else:
        apply_template(node, out)


def num_template(node, out):
    span = et.SubElement(out, 'span')
    class_num = get_num_style() + ' ' + 'LHS'
    span.text = node.text
    span.set('class', class_num)
    apply_template(node, span)


def listintroduction_template(node, out):
    apply_template(node, out)


def listwrapup_template(node, out):
    apply_template(node, out)


def p_template(node, out):
    p = et.SubElement(out, 'p')
    p.text = node.text
    copy_attributes(node, p)
    apply_template(node, p)


def elem_template(node, out):
    node_copy = copy.deepcopy(node)
    out.append(node_copy)


def table_elem_template(node, out):
    elem = et.SubElement(out, node.tag)
    copy_attributes(node, elem)
    # if node.attrib is not None:
    #     # elem.attrib = copy.deepcopy(node.attrib)
    #     for key, value in node.attrib.items():
    #         elem.set(key, value)
    apply_template(node, elem)


def copy_attributes(from_node, to_node):
    if from_node.attrib is not None and to_node is not None:
        for key, value in from_node.attrib.items():
            to_node.set(key, value)


def create_leg_html(input_path, output_path, ref_map_file_path):
    ref_resolver(ref_map_file_path)
    root_elem = et.parse(input_path).getroot()
    output = et.Element('div')
    output.set("class", "LegSnippet")
    start_parsing(root_elem, output)
    file = open(output_path, "w")
    file.write(et.tostring(output, pretty_print=True).decode("utf-8"))
    file.close()


def create_ft_html(input_path, output_path, ref_map_file_path):
    ref_resolver(ref_map_file_path)
    root_elem = et.parse(input_path).getroot()
    footnote = et.Element('div')
    footnote.set("class", "LegFootnote")
    add_footnotes(root_elem, footnote)
    file = open(output_path, "w")
    file.write(et.tostring(footnote, pretty_print=True).decode("utf-8"))
    file.close()

#
# input_path = "F:/coding/xml/50of2017/exp/10072017-note-exp.xml"
# temp_path = "F:/coding/xml/50of2017/exp/10072017-note-exptemp.xml"
# html_path = "F:/coding/xml/50of2017/exp/10072017-note-exp.html"
# footnote_path = "F:/coding/xml/50of2017/exp/10072017-note-expft.html"
# ref_resolver()
# root_elem = et.parse(input_path).getroot()
# output = et.Element('html')
# footnote = et.Element('html')
# # print(et.tostring(root, pretty_print=True).decode("utf-8"))
# add_footnotes(root_elem, footnote)
# # process_notes(root_elem)
#
# f = open(temp_path, "w")
# f.write(et.tostring(root_elem, pretty_print=True).decode("utf-8"))
# f.close()
#
# start_parsing(root_elem, output)
#
# f = open(html_path, "w")
# f.write(et.tostring(output, pretty_print=True).decode("utf-8"))
# f.close()
#
# f = open(footnote_path, "w")
# f.write(et.tostring(footnote, pretty_print=True).decode("utf-8"))
# f.close()

