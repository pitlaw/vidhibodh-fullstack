from django.shortcuts import render
from django.http import HttpResponse
import os
from django.conf import settings
from os import listdir
from os.path import exists
from datetime import datetime
from nltk.tokenize import TabTokenizer

from . import aknparser

context_index = {}
context_compare = {}
ref_map_file_path = ''
def index(request):
    global ref_map_file_path
    initial_uri = 'in-union/mof/dor/cbic/cus/tariff/2017/57'
    doclist_dict = {}
    doclist_file_path = os.path.join(settings.BASE_DIR, 'timeline/static/timeline/data/doclist.txt')
    ref_map_file_path = os.path.join(settings.BASE_DIR, 'timeline/static/timeline/data/refmap.txt')
    doclist_file = open(doclist_file_path, "r", encoding="utf8")
    lines = doclist_file.readlines()
    tk = TabTokenizer()
    i = 0

    for line in lines:
        i = i + 1
        tokens = tk.tokenize(line)
        doclist_dict[tokens[0]] = tokens[1]
        print("%s %s" % (tokens[0], tokens[1]))
    context_index['doclist_dict'] = doclist_dict

    x = initial_uri.split("/")
    print(x)
    return publish_doc(request, x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7])


def publish_doc(request, country, ministry, dept, board, domain, type, year, number):
    uri = format('%s/%s/%s/%s/%s/%s/%s/%s'%(country, ministry, dept, board, domain, type, year, number))
    datelist = versions(uri)
    context_index['datelist'] = datelist
    date = datelist[0]
    return publish_version(request, country, ministry, dept, board, domain, type, year, number, date)

def publish_version(request, country, ministry, dept, board, domain, type, year, number, date):
    uri = format('%s/%s/%s/%s/%s/%s/%s/%s' % (country, ministry, dept, board, domain, type, year, number))
    doc_html_url = uri_to_html_path(uri, date)
    doc_htmlft_url = uri_to_htmlft_path(uri, date)
    # If corresponding HTML files do not exist, create the files first
    if doc_html_exists(uri, date) is False:
        aknparser.create_leg_html(get_abs_xml_path(uri, date),
                                  get_abs_html_path(uri, date),
                                  ref_map_file_path)
        aknparser.create_ft_html(get_abs_xml_path(uri, date),
                                 get_abs_htmlft_path(uri, date),
                                 ref_map_file_path)
    context_index['date'] = date
    context_index['uri'] = uri
    context_index['doc_html_url'] = doc_html_url
    context_index['doc_htmlft_url'] = doc_htmlft_url

    print('In publish version!')
    return render(request, 'timeline/index.html', context_index)

# compare=1 for the original, compare=2 for the comparer
def compare_publish_version(request, country, ministry, dept, board, domain, type, year, number, date, compare):
    uri = format('%s/%s/%s/%s/%s/%s/%s/%s' % (country, ministry, dept, board, domain, type, year, number))
    doc_html_url = uri_to_html_path(uri, date)
    doc_htmlft_url = uri_to_htmlft_path(uri, date)
    # If corresponding HTML files do not exist, create the files first
    if doc_html_exists(uri, date) is False:
        aknparser.create_leg_html(get_abs_xml_path(uri, date),
                                  get_abs_html_path(uri, date),
                                  ref_map_file_path)
        aknparser.create_ft_html(get_abs_xml_path(uri, date),
                                 get_abs_htmlft_path(uri, date),
                                 ref_map_file_path)

    if compare == 1:
        context_compare['date1'] = date
        context_compare['datelist1'] = versions(uri)
        context_compare['uri1'] = uri
        context_compare['doc_html_url1'] = doc_html_url
        context_compare['doc_htmlft_url1'] = doc_htmlft_url
    elif compare == 2:
        context_compare['date2'] = date
        context_compare['datelist2'] = versions(uri)
        context_compare['uri2'] = uri
        context_compare['doc_html_url2'] = doc_html_url
        context_compare['doc_htmlft_url2'] = doc_htmlft_url

    return render(request, 'timeline/compare.html', context_compare)


def compare(request):
    context_compare['date1'] = context_index['date']
    context_compare['datelist1'] = context_index['datelist']
    context_compare['uri1'] = context_index['uri']
    context_compare['doc_html_url1'] = context_index['doc_html_url']
    context_compare['doc_htmlft_url1'] = context_index['doc_html_url']

    uri2 = request.POST['choice'][1:]
    datelist2 = versions(uri2)
    date2 = datelist2[0]
    x = uri2.split("/")

    return compare_publish_version(request, x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7], date2, 2)



def get_abs_xml_path(uri, date):
    doc_dir = os.path.join(settings.BASE_DIR, uri)
    return format('%s/%s.xml' % (doc_dir, date))


def get_abs_html_path(uri, date):
    rel_path = format('timeline/templates/timeline/legislation/%s' % (uri))
    html_dir = os.path.join(settings.BASE_DIR, rel_path)
    return format('%s/%s.html' % (html_dir, date))


def get_abs_htmlft_path(uri, date):
    rel_path = format('timeline/templates/timeline/legislation/%s' % (uri))
    html_dir = os.path.join(settings.BASE_DIR, rel_path)
    return format('%s/%sft.html' % (html_dir, date))


def doc_exists(uri, date):
    notif_dir = os.path.join(settings.BASE_DIR, uri)
    if os.path.isdir(notif_dir):
        path_to_file = format('%s/%s.xml' % (notif_dir, date))
        print(path_to_file)
        return exists(path_to_file)
    else:
        return False


def doc_html_exists(uri, date):
    rel_path = format('timeline/templates/timeline/legislation/%s' % (uri))
    html_dir = os.path.join(settings.BASE_DIR, rel_path)
    if os.path.isdir(html_dir):
        path_to_leg = format('%s/%s.html' % (html_dir, date))
        path_to_ft = format('%s/%sft.html' % (html_dir, date))
        print(path_to_leg)
        return exists(path_to_leg) and exists(path_to_ft)

    else:
        return False


def versions(uri):
    notif_dir = os.path.join(settings.BASE_DIR, uri)
    filelist = listdir(notif_dir)
    dateobj = []
    for file in filelist:
        filename = os.path.splitext(file)[0]
        datetime_object = datetime.strptime(filename, '%d%m%Y')
        dateobj.append(datetime_object)
        print(datetime_object.strftime('%d%m%Y'))

    dateobj.sort()
    datelist = []
    for date in dateobj:
        datelist.append(date.strftime('%d%m%Y'))

    return datelist


def uri_to_html_path(uri, date):
    return format('timeline/legislation/%s/%s.html' % (uri, date))


def uri_to_htmlft_path(uri, date):
    return format('timeline/legislation/%s/%sft.html' % (uri, date))