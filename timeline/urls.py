from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('timeline/compare/', views.compare, name='compare'),
    path('timeline//<slug:country>/<str:ministry>/<str:dept>/<str:board>/<str:domain>/'
         '<str:type>/<int:year>/<int:number>/', views.publish_doc, name='publish_doc'),
    path('timeline//<slug:country>/<str:ministry>/<str:dept>/<str:board>/<str:domain>/'
         '<str:type>/<int:year>/<int:number>/<str:date>',  views.publish_version, name='publish_version'),
    path('timeline/compare//<slug:country>/<str:ministry>/<str:dept>/<str:board>/<str:domain>/'
         '<str:type>/<int:year>/<int:number>/<str:date>/<int:compare>',
         views.compare_publish_version, name='compare_publish_version')
]